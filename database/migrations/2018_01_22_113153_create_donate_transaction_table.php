<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donate-transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('MerID');
            $table->string('AcqID');
            $table->string('OrderID');
            $table->string('ResponseCode');
            $table->string('ReasonCode');
            $table->string('ReasonCodeDesc');
            $table->string('ReferenceNo');
            $table->string('PaddedCardNo');
            $table->string('AuthCode');
            $table->string('ShipToToFirstName');
            $table->string('ShipToLastName');
            $table->string('Signature');
            $table->string('SignatureMethod');
            $table->string('SessionAmount');
            $table->string('SessionEmail');
            $table->string('SessionName');
            $table->string('SessionUserId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config');
    }
}
