<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            'id' => 1,
            'name' => 'AI_order_id',
            'value' => 0,
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
