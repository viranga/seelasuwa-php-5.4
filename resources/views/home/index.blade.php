@extends('layouts.app')

@section('content')
    <script>
        function validateForm() {
            var amount = angular.element('#amount').val();
            if (!((amount > 0) && (amount <= 500)))
            {
                //alert("Invalid value");
                return false;
            }
        }
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Donate now</div>

                    <div class="panel-body">
                        <div class="container1">
                            {{--<h2>Vertical (basic) Form</h2>--}}
                            <form onsubmit="return validateForm()" method="post">
                                {{ csrf_field() }}
                                <div class="form-group col-xs-3">
                                    <label for="amount">Amount ($):</label>
                                    {{--<input type="text" class="form-control" id="amount" placeholder="Enter Amount">--}}
                                    <input type="number" name="amount" id="amount" ng-model="example.value"
                                           min="0" max="500" class="form-control" required>
                                    <div class="invalid-amount" style="display: none">
                                        Please enter integer value between 0 and 500.
                                    </div>
                                </div>
                                <div class="form-group col-xs-12">
                                    <button type="submit" class="btn btn-default">Continue</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//code.angularjs.org/snapshot/angular.min.js"></script>
@endsection
