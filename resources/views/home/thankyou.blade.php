@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if ($request->ResponseCode == 1)
                    <div class="panel panel-default">
                        <div class="panel-heading">Successfully Donated</div>

                        <div class="panel-body">
                            <h3 align='center'>Thank you for your donation</h3>
                        </div>
                    </div>
                @else
                    <div class="panel panel-default">
                        <div class="panel-heading">Error!</div>

                        <div class="panel-body">
                            <div class="alert alert-danger" role="alert">
                                {{ $request->ReasonCodeDesc }} <br/>
                                Please contact site administrator.
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
