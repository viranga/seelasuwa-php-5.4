@extends('layouts.app')

@section('content')
    <script>


        function validateForm() {
//            var x = document.forms["myForm"]["fname"].value;
//            if (x == "") {
//                alert("Name must be filled out");
//                return false;
//            }

            var amount = angular.element('#amount').val();
            if (!((amount > 0) && (amount < 500)))
            {
                //alert("Invalid value");
                return false;
            }
        }
    </script>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Donate now</div>

                    <div class="panel-body">
                        <div class="container1">
                            {{--<h2>Vertical (basic) Form</h2>--}}
                            {{--<form onsubmit="return validateForm()" method="post">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<div class="form-group col-xs-3">--}}
                                    {{--<label for="amount">Amount:</label>--}}
                                    {{--<input type="text" class="form-control" id="amount" placeholder="Enter Amount">--}}
                                    {{--<input type="number" name="amount" id="amount" ng-model="example.value"--}}
                                           {{--min="0" max="500" class="form-control" required>--}}
                                {{--</div>--}}
                                {{--<div class="form-group col-xs-12">--}}
                                    {{--<button type="submit" class="btn btn-default">Continue</button>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        </div>


                        <form id="donation_form" method="post" action="https://www.hnbpg.hnb.lk/SENTRY/PaymentGateway/Application/ReDirectLink.aspx">
                            <?php
                            $pass = "zBp67DCt14581900415738".$orderId.$amount."840";
                            $enc =
                                base64_encode(pack('H*', sha1
                                ($pass)));
                            ?>
                                {{--<br>--}}
                                {{--<br>--}}
                                <h3 align='center'>Processing your Transaction...</h3>


                             <div align="center"> <input id='Version' type='hidden' name='Version' value="1.0.0">
                                 {{ csrf_field() }}
                                <input id='MerID' type='hidden' value="14581900" name='MerID' >
                                <input id='AcqID' type='hidden' value="415738" name='AcqID' >
                                <input id='MerRespURL' type='hidden' value="https://healthcaremonastery.org/donations/public/response" name='MerRespURL'>
                                <input id='PurchaseCurrency' type='hidden' value="840" name='PurchaseCurrency'>
                                <input id='PurchaseCurrencyExponent' type='hidden' value="2" name='PurchaseCurrencyExponent'>
                                <input id='OrderID' type='hidden' value="{{ $orderId }}" name='OrderID' >
                                <input id='SignatureMethod' type='hidden' value="SHA1" name='SignatureMethod'>
                                <input id='Signature' type='hidden' value="<?php echo $enc; ?>" name='Signature'>
                                <input id='CaptureFlag' type='hidden' value="A" name='CaptureFlag' >
                                <input id='PurchaseAmt' type='hidden' value="{{ $amount }}" name='PurchaseAmt' >
                                <input id='ShipToFirstName' type='hidden' value='{{ $firstName }}' name='ShipToFirstName' >
                                <input id='ShipToLastName' type='hidden' value='{{ $lastName }}' name='ShipToLastName' >
                                 {{--<h3 align="center"> Please click on the Submit button to continue processing.<br>--}}
                                     {{--<input type="submit" value="Submit"></h3>--}}
                             </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//code.angularjs.org/snapshot/angular.min.js"></script>

    <script>
        document.getElementById("donation_form").submit();
    </script>
@endsection
