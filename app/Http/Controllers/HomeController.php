<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','isVerified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$res = DB::table('config')->whereId(\Config::get('constants.DB_CONFIG_AUTO_INCREMENT_ORDER_ID'))->increment
        ('value');
        $orderId = '';
        if($res) {
            $result = DB::table('config')
                ->whereId(\Config::get('constants.DB_CONFIG_AUTO_INCREMENT_ORDER_ID'))
                ->pluck('value');
            $orderId = ($result)?$result[0]:0;
        }

        $pass = "zBp67DCt14581900415738SEELABBBB000000".$orderId."000000000100840";
        $enc = base64_encode(pack('H*', sha1($pass)));
*/
        //$aaa = sha1($pass);

        //return view('home', ['orderId' => $orderId, 'enc' => $enc]);
        return view('home/index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function donation(Request $request)
    {
        $res = DB::table('config')->whereId(\Config::get('constants.DB_CONFIG_AUTO_INCREMENT_ORDER_ID'))->increment('value');
        $orderId = '';
        if($res) {
            $result = DB::table('config')
                ->whereId(\Config::get('constants.DB_CONFIG_AUTO_INCREMENT_ORDER_ID'))
                ->pluck('value');
            $orderId = ($result)?$result[0]:0;
        }

        $pass = "zBp67DCt14581900415738SEELABBBB000000".$orderId."000000000100840";
        $enc = base64_encode(pack('H*', sha1($pass)));

        $amount = $request->input('amount');
        $request->session()->put('amount', $amount);

        $formattedAmount = sprintf('%010d', $amount);
        $formattedAmount = $formattedAmount.'00';
        $formattedOrderId = "SEELASUWA" . sprintf('%08d', $orderId);

        $user = Auth::user();
        $fullName = $user->name;
        $splitName = explode(' ', $fullName, 2); // Restricts it to only 2 values, for names like Billy Bob Jones
        $firstName = $splitName[0];
        $lastName = !empty($splitName[1]) ? $splitName[1] : '';

        return view('home/donation', ['orderId' => $formattedOrderId, 'enc' => $enc, 'amount' => $formattedAmount,
                'firstName' => $firstName, 'lastName' => $lastName]);
    }

    /**
     * Show the application response.
     *
     * @return \Illuminate\Http\Response                                                               $formattedAmount
     */
    public function response(Request $request)
    {
        $user = Auth::user();

        DB::table('donate-transactions')->insert(
            [
                'MerID' => $request->MerID,
                'AcqID' => $request->AcqID,
                'OrderID' => $request->OrderID,
                'ResponseCode' => $request->ResponseCode,
                'ReasonCode' => $request->ReasonCode,
                'ReasonCodeDesc' => $request->ReasonCodeDesc,
                'ReferenceNo' => $request->ReferenceNo,
                'PaddedCardNo' => $request->PaddedCardNo,
                'AuthCode' => $request->AuthCode,
                'ShipToToFirstName' => $request->ShipToToFirstName,
                'ShipToLastName' => $request->ShipToLastName,
                'Signature' => $request->Signature,
                'SignatureMethod' => $request->SignatureMethod,
                'SessionAmount' => $request->session()->get('amount'),
                'SessionEmail' => $user->email,
                'SessionName' => $user->name,
                'SessionUserId' => $user->id,
                'created_at' => new \DateTime(),
            ]
        );
        return view('home/thankyou', ['request' => $request]);
    }
}
