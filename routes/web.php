<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', 'Auth\LoginController@showLoginForm');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


//Route::post('register', 'Auth\RegisterController');
//Route::post('/register', 'Auth\RegisterController');

//Route::group(['prefix' => 'admin'], function () {
//    Voyager::routes();
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post ('/home', 'HomeController@donation')->name('home');

Route::post('/response', 'HomeController@response')->name('response');
